#include "eudaq/Configuration.hh"
#include "eudaq/Producer.hh"
#include "eudaq/Logger.hh"
#include "eudaq/RawDataEvent.hh"
#include "eudaq/Timer.hh"
#include "eudaq/Utils.hh"
#include "eudaq/OptionParser.hh"

#include <iostream>
#include <ostream>
#include <sstream>
#include <vector>
#include <string>
#include <filesystem>

#include "eudaq/Status.hh"
#include "eudaq/eudaq_version.h"

#include "Histo1d.h"
#include "Histo2d.h"
#include "Histo3d.h"

#include "HwController.h"

#include "AllAnalyses.h"
#include "AllHwControllers.h"
#include "AllHistogrammers.h"
#include "AllChips.h"
#include "AllProcessors.h"
#include "AllStdActions.h"

#include "StdHistogrammer.h"
#include "StdAnalysis.h"

#include "storage.hpp"
#include "yarr.h"
#include "logging.h"
#include "LoggingConfig.h"

#include "Bookkeeper.h"
#include "ScanBase.h"
#include "ScanFactory.h"
#include "ScanOpts.h"
#include "ScanHelper.h"

#include "Rd53aCfg.h"
#include "Rd53bCfg.h"

#include "yarr_producer_version.h"

static const std::string EVENT_TYPE = "Yarr";
static const std::string YARR_EVENT_VERSION = "2.1.0";


class YarrProducer;

class EudetArchiver : public HistogramAlgorithm {
    public:
        EudetArchiver(YarrProducer *prod, unsigned id, unsigned numOfTrig) : HistogramAlgorithm() {
            r = NULL;
            m_prod = prod;
            m_curBlock = 0;
            m_id = id;
            m_curEvent = new std::vector<char>;
            lastL1 = numOfTrig-1;
            last_nHits_it = 0; 
            m_numOfTrig = numOfTrig;
        };
        ~EudetArchiver();


        virtual void processEvent(FrontEndData *data) = 0;
        void create(LoopStatus &stat) {}
    protected:
        YarrProducer *m_prod;
        std::vector<char> *m_curEvent;
        unsigned lastL1;
        unsigned last_nHits_it; 
        int m_curBlock;
        unsigned m_numOfTrig;
        unsigned m_id;
        const std::array<int, 32> l1ToTag = {{0,0,1,1,1,1,2,2,2,2,3,3,3,3,4,4,4,4,5,5,5,5,6,6,6,6,7,7,7,7,0,0}};
};

class EudetArchiverRD53a : public EudetArchiver {
    public:
        EudetArchiverRD53a(YarrProducer *prod, unsigned channel, unsigned numOfTrig) : EudetArchiver(prod, channel, numOfTrig) {
        };
        ~EudetArchiverRD53a() {};

        void processEvent(FrontEndData *data) override;

};


class EudetArchiverRD53b : public EudetArchiver {
    public:
        EudetArchiverRD53b(YarrProducer *prod, unsigned channel, unsigned numOfTrig) : EudetArchiver::EudetArchiver(prod, channel, numOfTrig) {
             m_eventNumber = 0;
        };
        ~EudetArchiverRD53b() {};


        void processEvent(FrontEndData *data) override;

    private:
        unsigned m_eventNumber;

};


class YarrProducer : public eudaq::Producer {
    public:

        YarrProducer(HwController *hwCtrl, Bookkeeper *bookie, ScanBase *scan, std::string chipType, const unsigned prodId, const std::string & runcontrol, unsigned numOfTrig, std::string connectivity_config_path, std::string connectivity_config, std::string chip_configs_paths, std::vector<std::string> chip_configs, std::string hw_controller_config_path, std::string hw_controller_config, std::string scan_config_path, std::string scan_config, std::string module_config_path, std::string module_config ) 
            : eudaq::Producer("YarrProducer"+std::to_string(prodId), runcontrol), m_hwCtrl(hwCtrl), m_bookie(bookie), m_scan(scan), 
            m_chipType(chipType), m_prodId(prodId), m_run(0), m_ev(0), stopping(false), done(false), m_numOfTrig(numOfTrig), m_connectivity_config_path(connectivity_config_path), m_connectivity_config(connectivity_config), m_chip_configs_paths(chip_configs_paths), m_chip_configs(chip_configs), m_hw_controller_config_path(hw_controller_config_path), m_hw_controller_config(hw_controller_config), m_scan_config_path(scan_config_path), m_scan_config(scan_config), m_module_config_path(module_config_path), m_module_config(module_config)    {
                m_trigLatency = 100;
                m_event = NULL;
            }

        // This gets called whenever the DAQ is initialised
        virtual void OnInitialise(const eudaq::Configuration & init) {
            try {
                ScanOpts scanOpts;
                spdlog::set_pattern(scanOpts.defaultLogPattern);
                std::cout << __PRETTY_FUNCTION__ << std::endl;
                std::cout << "Reading: " << init.Name() << std::endl;


                // Init stuff
                m_scan->init();

                // At the end, set the ConnectionState that will be displayed in the Run Control.
                // and set the state of the machine.
                SetConnectionState(eudaq::ConnectionState::STATE_UNCONF, "Initialised (" + init.Name() + ")");
            } 
            catch (...) {
                // Message as cout in the terminal of your producer
                std::cout << "Unknown exception" << std::endl;
                // Message to the LogCollector
                EUDAQ_ERROR("YarrEudaqProducer unknown exception during initialisation");
                // Otherwise, the State is set to ERROR
                SetConnectionState(eudaq::ConnectionState::STATE_ERROR, "Initialisation Error");
            }
        }

        // This gets called whenever the DAQ is configured
        virtual void OnConfigure(const eudaq::Configuration & config) {
            try {
                std::cout << __PRETTY_FUNCTION__ << std::endl;
                std::cout << "Reading: " << config.Name() << std::endl;


                m_setupName = config.Get("setupName", "YARR");
                m_trigLatency = config.Get("latency", 100);
                m_deadtime = config.Get("deadtime", 200);

                std::cout << std::endl;
                std::cout << "\033[1;31m#################\033[0m" << std::endl;
                std::cout << "\033[1;31m# Configure FEs #\033[0m" << std::endl;
                std::cout << "\033[1;31m#################\033[0m" << std::endl;

                std::chrono::steady_clock::time_point cfg_start = std::chrono::steady_clock::now();
                for (unsigned id=0; id<m_bookie->getNumOfEntries(); id++) {
                    FrontEnd *fe = m_bookie->getEntry(id).fe;
                    std::cout << "-> Configuring " << dynamic_cast<FrontEndCfg*>(fe)->getName() << std::endl;
                    // Select correct channel
                    m_hwCtrl->setCmdEnable(dynamic_cast<FrontEndCfg*>(fe)->getTxChannel());
                    // Configure
                    fe->configure();
                    // Wait for fifo to be empty
                    std::this_thread::sleep_for(std::chrono::microseconds(100));
                    while(!m_hwCtrl->isCmdEmpty());
                }
                std::chrono::steady_clock::time_point cfg_end = std::chrono::steady_clock::now();
                std::cout << "-> All FEs configured in " 
                    << std::chrono::duration_cast<std::chrono::milliseconds>(cfg_end-cfg_start).count() << " ms !" << std::endl;

                // Wait for rx to sync with FE stream
                std::this_thread::sleep_for(std::chrono::microseconds(1000));
                m_hwCtrl->flushBuffer();
                for (unsigned id=0; id<m_bookie->getNumOfEntries(); id++) {
                    FrontEnd *fe = m_bookie->getEntry(id).fe;
                    std::cout << "  -> Checking com " << dynamic_cast<FrontEndCfg*>(fe)->getName() << std::endl;
                    // Select correct channel
                    m_hwCtrl->setCmdEnable(dynamic_cast<FrontEndCfg*>(fe)->getTxChannel());
                    m_hwCtrl->setRxEnable(dynamic_cast<FrontEndCfg*>(fe)->getRxChannel());
                    // Configure
                    if (fe->checkCom() != 1) {
                        std::cout << "#ERROR# Can't establish communication, aborting!" << std::endl;
                        throw("Communication error!");
                    }
                    std::cout <<   "   ... success!" << std::endl;
                }

                std::this_thread::sleep_for(std::chrono::microseconds(100));
                // Enable all active channels
                std::cout << "-> Enabling Tx channels: " << std::endl;
                for (uint32_t channel : m_bookie->getTxMask()) {
                    std::cout << "  ... " << channel  << std::endl;
                }
                m_hwCtrl->setCmdEnable(m_bookie->getTxMask());

                // Configure hardware 
                std::cout << "-> Setting up triggerlogic ..." << std::endl;
                m_hwCtrl->resetTriggerLogic();
                m_hwCtrl->setTriggerLogicMask(0x010); // Eudet TLU
                m_hwCtrl->setTriggerLogicMode(MODE_EUDET_TAG);

                // At the end, set the ConnectionState that will be displayed in the Run Control.
                // and set the state of the machine.
                std::cout << "    ... configured!" << std::endl;
                SetConnectionState(eudaq::ConnectionState::STATE_CONF, "Configured (" + config.Name() + ")");
            } 
            catch (...) {
                // Otherwise, the State is set to ERROR
                printf("Unknown exception\n");
                // commenting out to give the possibility to re-configure in case of a failure instead of starting from scratch
                // SetConnectionState(eudaq::ConnectionState::STATE_ERROR, "Configuration Error");
            }
        }

        // This gets called whenever a new run is started
        // It receives the new run number as a parameter
        // And sets the event number to 0 (internally)
        virtual void OnStartRun(unsigned param) {
            try {

                m_run = param;
                m_ev = 0;

                std::cout << "##################" << std::endl;
                std::cout << "### Start Run: ### " << m_run << std::endl;
                std::cout << "##################" << std::endl;

                m_hwCtrl->resetTriggerLogic();

                std::cout << "Init BORE" << std::endl;
                // It must send a BORE (Begin-Of-Run Event) to the Data Collector
                eudaq::RawDataEvent bore(eudaq::RawDataEvent::BORE(EVENT_TYPE, m_run));
                bore.SetTag("YARREVENTVERSION", YARR_EVENT_VERSION);
                bore.SetTag("PRODID", m_prodId);
                bore.SetTag("DUTTYPE", m_chipType);
                // bore.SetTag("TRIGLATENCY", m_trigLatency);
                bore.SetTag("TRIGLATENCY", "DEFINEDINCONFIG");
                // bore.SetTag("DEADTIME", m_deadtime);
                bore.SetTag("DEADTIME", "DEFINEDINCONFIG");
                // bore.SetTag("NUMOFTRIG", m_numOfTrig);
                bore.SetTag("NUMOFTRIG", "DEFINEDINCONFIG");
                bore.SetTag("CONNECTIVITYCONFIGPATH", m_connectivity_config_path);
                bore.SetTag("CONNECTIVITYCONFIG", m_connectivity_config);
                bore.SetTag("CHIPCONFIGSPATHS", m_chip_configs_paths);
                for (size_t i = 0; i < m_chip_configs.size(); ++i)
                {
                    std::string tag_name = "CHIPCONFIG" + std::to_string(i);
                    bore.SetTag(tag_name, m_chip_configs[i]);
                };
                bore.SetTag("HWCONTRCONFIGPATH", m_hw_controller_config_path);
                bore.SetTag("HWCONTRCONFIG", m_hw_controller_config);
                bore.SetTag("SCANCONFIGPATH", m_scan_config_path);
                bore.SetTag("SCANCONFIG", m_scan_config);
                bore.SetTag("MODULECONFIGPATH", m_module_config_path);
                bore.SetTag("MODULECONFIG", m_module_config);
                
                // YARR properties
                json yarrInfo = yarr::version::get();
                bore.SetTag("YARR_GIT_BRANCH", static_cast<std::string>(yarrInfo["git_branch"]));
                bore.SetTag("YARR_GIT_TAG", static_cast<std::string>(yarrInfo["git_tag"]));
                bore.SetTag("YARR_GIT_HASH", static_cast<std::string>(yarrInfo["git_hash"]));
                bore.SetTag("YARR_GIT_DATE", static_cast<std::string>(yarrInfo["git_date"]));
                bore.SetTag("YARR_GIT_SUBJECT", static_cast<std::string>(yarrInfo["git_subject"]));
                
                // YARR PRODUCER properties
                json yarrProducerInfo = yarr_producer::version::get();
                bore.SetTag("YARR_PRODUCER_GIT_BRANCH", static_cast<std::string>(yarrInfo["git_branch"]));
                bore.SetTag("YARR_PRODUCER_GIT_TAG", static_cast<std::string>(yarrInfo["git_tag"]));
                bore.SetTag("YARR_PRODUCER_GIT_HASH", static_cast<std::string>(yarrInfo["git_hash"]));
                bore.SetTag("YARR_PRODUCER_GIT_DATE", static_cast<std::string>(yarrInfo["git_date"]));
                bore.SetTag("YARR_PRODUCER_GIT_SUBJECT", static_cast<std::string>(yarrInfo["git_subject"]));       
                
                // EUDAQ underlying YARR PRODUCER properties
                bore.SetTag("EUDAQ_YARR_PRODUCER_GIT_BRANCH", eudaq::version::getEudaqGitBranch());
                bore.SetTag("EUDAQ_YARR_PRODUCER_GIT_TAG", eudaq::version::getEudaqGitTag());
                bore.SetTag("EUDAQ_YARR_PRODUCER_GIT_HASH", eudaq::version::getEudaqGitHash());
                bore.SetTag("EUDAQ_YARR_PRODUCER_GIT_DATE", eudaq::version::getEudaqGitDate());
                bore.SetTag("EUDAQ_YARR_PRODUCER_GIT_SUBJECT", eudaq::version::getEudaqGitSubject());                         


                for (unsigned id=0; id<m_bookie->getNumOfEntries(); id++ ) {
                    for (auto &j : m_dataMap[id]) {
                        delete j.second;
                    }
                    m_dataMap[id].clear();
                }
                m_dataMap.clear();
                m_eventCnt.clear();
                
                // Create data processor
                std::cout << "-> Init Raw data processors .." << std::endl;
                ScanHelper::buildRawDataProcs(m_procs, *m_bookie, m_chipType);

                std::cout << "-> Init Histogrammer" << std::endl << std::flush;
                for (unsigned id=0; id<m_bookie->getNumOfEntries(); id++) {
                    FrontEnd *fe = m_bookie->getEntry(id).fe;
                    m_histogrammers[id].reset( new HistogrammerProcessor );
                    auto& histogrammer = static_cast<HistogrammerProcessor&>( *(m_histogrammers[id]) );
                    histogrammer.connect(&fe->clipData, &fe->clipHisto);
                    // This is where the raw data is saved
                    // TODO make folder
                    auto archiver = std::unique_ptr<HistogramAlgorithm>(new DataArchiver);
                    dynamic_cast<DataArchiver*>(archiver.get())->open("data/" + dynamic_cast<FrontEndCfg*>(fe)->getName() + "_" + this->toString(m_run, 6) + ".raw");
                    histogrammer.addHistogrammer(std::move(archiver));

                    if(m_chipType=="RD53A") {
                      histogrammer.addHistogrammer(std::move(std::unique_ptr<HistogramAlgorithm>(new EudetArchiverRD53a(this, id, m_numOfTrig))));
                    } else if(m_chipType=="RD53B") {
                      histogrammer.addHistogrammer(std::move(std::unique_ptr<HistogramAlgorithm>(new EudetArchiverRD53b(this, id, m_numOfTrig))));
                    } else {
                      std::cerr << __PRETTY_FUNCTION__ << " : invalid chip type: " << m_chipType << std::endl;                      
                    }
                    histogrammer.setMapSize(fe->geo.nCol, fe->geo.nRow);
                }

                std::cout << "-> Init ScanLoop" << std::endl;
                m_scan->preScan();

                std::cout << "-> Start-up processors" << std::endl;
                for (unsigned id=0; id<m_bookie->getNumOfEntries(); id++) {
                    FrontEnd *fe = m_bookie->getEntry(id).fe;
                    if (fe->isActive()) {

                        m_histogrammers[id]->init();
                        m_histogrammers[id]->run();

                        m_procs[id]->init();
                        m_procs[id]->run();

                        std::cout << " .. started threads of Fe with ID " << id << std::endl;
                    }
                }

                std::cout << "-> Enabling Rx channels: " << std::endl;
                m_hwCtrl->setRxEnable(m_bookie->getRxMask());
                for (uint32_t channel : m_bookie->getRxMask()) {
                    std::cout << "  ... " << channel  << std::endl;
                }

                // get the data necessary for modules
                struct chipInfo {
                     std::string name;
                     unsigned int chipId; 
                     unsigned int rx;
                     std::string moduleType;
                     std::string sensorType;
                     std::string pcbType;
                     unsigned int chipLocationOnModule;
                     unsigned int internalModuleIndex;
                     std::string moduleName;
                     std::string moduleID;
                     };
                     
                std::vector<chipInfo>  chip_info_by_uid;

                for (unsigned id=0; id<m_bookie->getNumOfEntries(); id++) {
                    FrontEnd *fe = m_bookie->getEntry(id).fe;
                    chipInfo currentChipInfo;
                    if(m_chipType=="RD53A") {
                        currentChipInfo.name = dynamic_cast<Rd53aCfg*>(fe)->getName();
                        currentChipInfo.chipId = dynamic_cast<Rd53aCfg*>(fe)->getChipId();
                        currentChipInfo.rx = dynamic_cast<Rd53aCfg*>(fe)->getRxChannel();                                                       
                    } else if(m_chipType=="RD53B") {
                        currentChipInfo.name = dynamic_cast<Rd53bCfg*>(fe)->getName();
                        currentChipInfo.chipId = dynamic_cast<Rd53bCfg*>(fe)->getChipId();
                        currentChipInfo.rx = dynamic_cast<Rd53bCfg*>(fe)->getRxChannel();                                                       
                    };
                    chip_info_by_uid.push_back(currentChipInfo);
                }

               json modConfig;
               try {
                  json::parse(m_module_config.c_str(),modConfig);
               } catch(json::parse_error &e) {
                  std::cerr << __PRETTY_FUNCTION__ << " : " << e.what() << std::endl;
               }
               for (unsigned i=0; i<modConfig["modules"].size(); i++) {
                  std::cout << "Loading module #" << i << " of " << modConfig["modules"].size() << " modules" << std::endl;

                  json modConfigSingle = modConfig["modules"][i];
                  for (unsigned j=0; j<modConfigSingle["chipNames"].size(); j++) {                  
                     std::string chipNameLocal = modConfigSingle["chipNames"][j];
                     auto it = std::find_if(chip_info_by_uid.begin(),chip_info_by_uid.end(), [&chipNameLocal](const chipInfo& val){ return val.name == chipNameLocal; });
                     if (it!= chip_info_by_uid.end()) {
                        it->moduleType = modConfigSingle["moduleType"];
                        it->sensorType = modConfigSingle["sensorType"];
                        it->pcbType = modConfigSingle["pcbType"];
                        it->chipLocationOnModule = j+1; // chip positions are 1,2,3,4
                        it->internalModuleIndex = i;
                        it->moduleName = modConfigSingle["moduleName"];
                        it->moduleID = modConfigSingle["moduleID"];
                     }
                  }
                }
                
                //Now turn the collected info about modules into a json
                json moduleChipInfoJson;
                moduleChipInfoJson["uid"]=json::array();
                for(std::size_t uidIndex = 0; uidIndex < chip_info_by_uid.size(); ++uidIndex) {
		   moduleChipInfoJson["uid"][uidIndex]["name"] = chip_info_by_uid[uidIndex].name;
		   moduleChipInfoJson["uid"][uidIndex]["chipId"] = chip_info_by_uid[uidIndex].chipId;
		   moduleChipInfoJson["uid"][uidIndex]["rx"] = chip_info_by_uid[uidIndex].rx;
		   moduleChipInfoJson["uid"][uidIndex]["moduleType"] = chip_info_by_uid[uidIndex].moduleType;
		   moduleChipInfoJson["uid"][uidIndex]["sensorType"] = chip_info_by_uid[uidIndex].sensorType;
		   moduleChipInfoJson["uid"][uidIndex]["pcbType"] = chip_info_by_uid[uidIndex].pcbType;
		   moduleChipInfoJson["uid"][uidIndex]["chipLocationOnModule"] = chip_info_by_uid[uidIndex].chipLocationOnModule;	   
		   moduleChipInfoJson["uid"][uidIndex]["internalModuleIndex"] = chip_info_by_uid[uidIndex].internalModuleIndex;
		   moduleChipInfoJson["uid"][uidIndex]["moduleName"] = chip_info_by_uid[uidIndex].moduleName;
		   moduleChipInfoJson["uid"][uidIndex]["moduleID"] = chip_info_by_uid[uidIndex].moduleID;
		}
                
                std::string moduleChipInfoSerialized;
                moduleChipInfoJson.dump(moduleChipInfoSerialized);
                bore.SetTag("MODULECHIPINFO", moduleChipInfoSerialized );
                
                std::cout << "-> send BORE" << std::endl << std::flush;
                // Send the event to the Data Collector
                SendEvent(bore);                
                
                std::this_thread::sleep_for(std::chrono::milliseconds(100));


                // Going into run mode
                std::cout << "-> Going into runmode!" << std::endl;
                runThread.push_back(std::thread(&ScanBase::run, m_scan));


                // At the end, set the ConnectionState that will be displayed in the Run Control.
                SetConnectionState(eudaq::ConnectionState::STATE_RUNNING, "Running");
            } 
            catch (...) {
                // Otherwise, the State is set to ERROR
                printf("Unknown exception\n");
                SetConnectionState(eudaq::ConnectionState::STATE_ERROR, "Starting Error");
            }
        }

        // This gets called whenever a run is stopped
        virtual void OnStopRun() {
            try {

                std::cout << "####################" << std::endl;
                std::cout << "### Stopping Run ###" << std::endl;
                std::cout << "####################" << std::endl;

                std::this_thread::sleep_for(std::chrono::milliseconds(100));

                // Kill the run loop (gracefully)
                ((StdDataGatherer*)m_scan->getLoop(1).get())->kill(); // Should be data gatherer
                std::this_thread::sleep_for(std::chrono::milliseconds(20));

                // Wait for scan to exit
                std::cout << "... waiting for run thread ... " << std::endl;
                for (auto &n : runThread) 
                    n.join();
                
                m_scan->postScan();

                // Wait for processor
                eudaq::mSleep(20);
                
                for (unsigned id=0; id<m_bookie->getNumOfEntries(); id++ ) {
                    FrontEnd *fe = m_bookie->getEntry(id).fe;
                    if (fe->isActive()) {
                        fe->clipRawData.finish();
                    }
                }
                
                for( auto& proc : m_procs ) {
                    proc.second->join();
                }
                std::cout << "-> Processor done, waiting for histogrammer ..." << std::endl;

                //HistogrammerProcessor::processorDone = true;
                eudaq::mSleep(20);

                for (unsigned id=0; id<m_bookie->getNumOfEntries(); id++ ) {
                    FrontEnd *fe = m_bookie->getEntry(id).fe;
                    if (fe->isActive()) {
                        fe->clipData.finish();
                    }
                }

                // Join histogrammers
                for( auto& histogrammer : m_histogrammers ) {
                    histogrammer.second->join();
                }
                std::cout << "-> Histogrammer done" << std::endl;
                std::cout << "-> All rogue threads joined!" << std::endl;

                m_hwCtrl->setRxEnable(0x0);

                std::cout << "Event count per id: " << std::endl;
                for (unsigned id=0; id<m_bookie->getNumOfEntries(); id++) {
                    std::cout << "  ... " << id  << " -> " << m_eventCnt[id] << std::endl;
                }

                // Set a flag to signal to the polling loop that the run is over and it is in the stopping process
                stopping = true;

                std::cout << "-> Cleanup" << std::endl;

                // Cleanup
                runThread.clear();

                for( auto& histogrammer : m_histogrammers ) {
                    histogrammer.second.reset(NULL);
                }
                
                // Save backup config
                std::cout << "   -> Backup config" << std::endl;
                for (unsigned id=0; id<m_bookie->getNumOfEntries(); id++ ) {
                    FrontEnd *fe = m_bookie->getEntry(id).fe;
                    std::fstream ocfg_file(("data/" + dynamic_cast<FrontEndCfg*>(fe)->getName() + "_" + this->toString(m_run, 6) + ".json").c_str(), std::ios::out);
                    json cfg;
                    dynamic_cast<FrontEndCfg*>(fe)->writeConfig(cfg);
                    ocfg_file << std::setw(4) << cfg;
                    ocfg_file.close();
                }

                //Empty containers
                std::cout << "   -> Empty data containers." << std::endl;
                for (unsigned id=0; id<m_bookie->getNumOfEntries(); id++ ) {
                    FrontEnd *fe = m_bookie->getEntry(id).fe;
                    while(!fe->clipData.empty())
                        fe->clipData.popData().reset();
                    while(!fe->clipHisto.empty())
                        fe->clipHisto.popData().reset();

                    // Reset done flags
                    fe->clipData.reset();
                    fe->clipHisto.reset();
                }

                // Send EORE
                std::cout << "Got " << m_ev << "events!" << std::endl;
                std::cout << " -> send EORE" << std::endl;
                SendEvent(eudaq::RawDataEvent::EORE(EVENT_TYPE, m_run, m_ev));

                // At the end, set the ConnectionState that will be displayed in the Run Control.
                // Due to the definition of FSM, it should go to STATE_CONF. 
                if (m_connectionstate.GetState() != eudaq::ConnectionState::STATE_ERROR)
                    SetConnectionState(eudaq::ConnectionState::STATE_CONF);
            } 
            catch (...) {
                // Otherwise, the State is set to ERROR
                printf("Unknown exception\n");
                SetConnectionState(eudaq::ConnectionState::STATE_ERROR, "Stopping Error");
            }
        }

        // This gets called when the Run Control is terminating,
        // we should also exit.
        virtual void OnTerminate() {
            std::cout << "-> Terminating..." << std::endl;
            done = true;
        }

        // This loop is running in the main
        void ReadoutLoop() {
            try {
                // Loop until Run Control tells us to terminate using the done flag
                while (!done) {
                    // If the Producer is not in STATE_RUNNING, it will restart the loop
                    if (GetConnectionState() != eudaq::ConnectionState::STATE_RUNNING) {
                        // Now sleep for a bit, to prevent chewing up all the CPU
                        eudaq::mSleep(20);
                        // Then restart the loop
                        continue;
                    }
                } 
            }
            catch (...) {
                // Otherwise, the State is set to ERROR
                printf("Unknown exception\n");
                SetConnectionState(eudaq::ConnectionState::STATE_ERROR, "Error during running");
            }
        }

        void incEventCount() {
            ++m_ev;
        }

        void sendHitData(unsigned arg_id, std::vector<char> *data) {

            unsigned eventId = m_eventCnt[arg_id];
            
            // Buffer hit data for event
            m_dataMap[arg_id][eventId] = data;

            // Check if all histogrammers have send data for this event
            bool receivedAll = true;
            for (unsigned id=0; id<m_bookie->getNumOfEntries(); id++ ) {
                if (m_dataMap[id].find(eventId) == m_dataMap[id].end())
                    receivedAll = false;
            }

            // Send when all histogrammer have pushed the event
            if (receivedAll) {

                // Create new event
                m_event = new eudaq::RawDataEvent(EVENT_TYPE, m_run, m_ev);
                m_event->SetTag("EventNumber", m_ev);
                m_event->SetTag("PRODID", m_prodId);
                
                // Add al blocks
                for (unsigned id=0; id<m_bookie->getNumOfEntries(); id++ ) {
                    m_event->AddBlock(id, *m_dataMap[id][eventId]);
                    delete m_dataMap[id][eventId];
                    m_dataMap[id].erase(eventId);
                }

                // Send the event to the Data Collector 
                this->SendEvent(*m_event);
                delete m_event;
                // Create new event

                this->incEventCount();
                if (m_ev<10 || m_ev%1000 == 0)
                    std::cout << "~~ Sent event #: " << m_ev << std::endl;
            }

            // Increase per channel event counter
            m_eventCnt[arg_id]++;
        }

        std::mutex eventMutex;

    private:
        // YARR Objects
        HwController *m_hwCtrl;
        Bookkeeper *m_bookie;
        ScanBase *m_scan;

        std::map<unsigned, std::unique_ptr<DataProcessor> > m_histogrammers;
        std::map<unsigned, std::unique_ptr<DataProcessor> > m_procs;
        FeedbackClipboardMap m_fbData;
        std::vector<std::thread> runThread;

        // YARR producer config
        unsigned m_prodId; // To distinguish different systems
        std::string m_chipType;

        std::string m_connectivity_config_path;
        std::string m_connectivity_config;

        std::string m_chip_configs_paths;
        std::vector<std::string> m_chip_configs;

        std::string m_hw_controller_config_path;
        std::string m_hw_controller_config;      	

        std::string m_scan_config_path;
        std::string m_scan_config;           	
        
        std::string m_module_config_path;
        std::string m_module_config;            

        // (could be but not yet implemented) Setable via config from run control
        std::string m_setupName;
        unsigned m_trigLatency;
        unsigned m_deadtime;
        unsigned m_numOfTrig;

        unsigned m_run, m_ev;
        bool stopping, done;

        // Helper functions
        std::string toString(int value,int digitsCount)
        {
            std::ostringstream os;
            os<<std::setfill('0')<<std::setw(digitsCount)<<value;
            return os.str();
        }

        // Event collector
        eudaq::RawDataEvent *m_event;
        std::map<unsigned, std::map<unsigned, std::vector<char>*>> m_dataMap;
        std::map<unsigned, unsigned> m_eventCnt;

};

// ###############################
// ####          MAIN         ####
// ###############################
int main(int /*argc*/, const char ** argv) {
    eudaq::OptionParser op("YARR EUDAQ Producer", "1.0", "");
    eudaq::Option<std::string> rctrl(op, "r", "runcontrol", "tcp://localhost:44000", "address", "The address of the RunControl.");
    eudaq::Option<std::string> level(op, "l", "log-level", "NONE", "level", "The minimum level for displaying log messages locally");
    eudaq::Option<std::string> conCfg(op, "c", "connectivity", "", "string", "Connectivity Config");
    eudaq::Option<std::string> modCfg(op, "m", "modules", "", "string", "Modules Config");    
    eudaq::Option<std::string> ctrlCfgPath(op, "h", "controller", "", "string", "HArdware Controller Config");
    eudaq::Option<std::string> scanType(op, "s", "scan", "", "string", "Scan Config");
    eudaq::Option<std::string> outputDir(op, "o", "output", "data", "string", "Output Directory");
    eudaq::Option<unsigned> prodId(op, "p", "prod-id", 0, "unsigned", "Producer ID");

    std::string defaultLogPattern = "[%T:%e]%^[%=8l][%=15n]:%$ %v";
    spdlog::set_pattern(defaultLogPattern);
    json j; // empty
    j["pattern"] = defaultLogPattern;
    j["log_config"][0]["name"] = "all";
    j["log_config"][0]["level"] = "info";
    logging::setupLoggers(j);

    try {
        // This will look through the command-line arguments and set the options
        op.Parse(argv);
        // Set the Log level for displaying messages based on command-line
        EUDAQ_LOG_LEVEL(level.Value());

        // #############################
        std::cout << "\033[1;31m#################\033[0m" << std::endl;
        std::cout << "\033[1;31m# YARR Producer #\033[0m" << std::endl;
        std::cout << "\033[1;31m#################\033[0m" << std::endl;
        EUDAQ_INFO("YarrProducer: starting");
        std::cout << "-> Producer connected to " << rctrl.Value() << std::endl;
        // Timestamp
        std::time_t now = std::time(NULL);
        struct tm *lt = std::localtime(&now);
        char timestamp[20];
        strftime(timestamp, 20, "%F_%H:%M:%S", lt);
        std::cout << "Timestamp: " << timestamp << std::endl;

        // Output directory
        std::error_code directory_creation_error_code;
	std::filesystem::create_directories(outputDir.Value(), directory_creation_error_code);

        if(directory_creation_error_code){
            std::cerr << "Error creating output directory - plots might not be saved: " << directory_creation_error_code.message() << std::endl;
        }

        // Init hardware controller
        std::cout << std::endl;
        std::cout << "\033[1;31m#################\033[0m" << std::endl;
        std::cout << "\033[1;31m# Init Hardware #\033[0m" << std::endl;
        std::cout << "\033[1;31m#################\033[0m" << std::endl;
        EUDAQ_INFO("YarrProducer: initialising");

        std::unique_ptr<HwController> hwCtrl = nullptr;
        std::stringstream ctrlCfgFile_buffer;
        if (ctrlCfgPath.Value() == "") {
            std::cout << "#ERRROR# No controller config given, aborting." << std::endl;
            return -1;
        } else {
            // Open controller config file
            std::cout << "-> Opening controller config: " << ctrlCfgPath.Value() << std::endl;
            std::ifstream ctrlCfgFile(ctrlCfgPath.Value());
            if (!ctrlCfgFile) {
                std::cerr <<"#ERROR# Cannot open controller config file: " << ctrlCfgPath.Value() << std::endl;
                return -1;
            }

            ctrlCfgFile_buffer << ctrlCfgFile.rdbuf();
            ctrlCfgFile.clear();
            ctrlCfgFile.seekg(0);

            json ctrlCfg;
            try {
                ctrlCfg = json::parse(ctrlCfgFile);
            } catch (json::parse_error &e) {
                std::cerr << "#ERROR# Could not parse config: " << e.what() << std::endl;
                return 0;
            }
            std::string controller = ctrlCfg["ctrlCfg"]["type"];

            hwCtrl = StdDict::getHwController(controller);

            if(hwCtrl) {
                std::cout << "-> Found config for controller " << controller << std::endl;

                hwCtrl->loadConfig(ctrlCfg["ctrlCfg"]["cfg"]);
            } else {
                std::cerr << "#ERROR# Unknown config type: " << ctrlCfg["ctrlCfg"]["type"] << std::endl;
                std::cout << " Known HW controllers:\n";
                for(auto &h: StdDict::listHwControllers()) {
                    std::cout << "  " << h << std::endl;
                }
                std::cerr << "Aborting!" << std::endl;
                return -1;
            }
        }
        // Disable trigger in-case
        hwCtrl->setTrigEnable(0);

        // Bookie
        Bookkeeper bookie(&*hwCtrl, &*hwCtrl);

        std::map<unsigned, std::string> feCfgMap;

        std::cout << "\033[1;31m#######################\033[0m" << std::endl
            << "\033[1;31m##  Loading Configs  ##\033[0m" << std::endl
            << "\033[1;31m#######################\033[0m" << std::endl;

        int success = 0;
        std::string chipType;

        // Loop over setup files
        if (conCfg.Value() == "") {
            std::cout << "#ERRROR# No controller config given, aborting." << std::endl;
            return -1;
        }
        std::cout << "Opening global config: " << conCfg.Value() << std::endl;
        std::ifstream gConfig(conCfg.Value());
        std::stringstream gConfig_buffer;
        gConfig_buffer << gConfig.rdbuf();
        gConfig.clear();
        gConfig.seekg(0);	

        json config;
        try {
            config = json::parse(gConfig);
        } catch(json::parse_error &e) {
            std::cerr << __PRETTY_FUNCTION__ << " : " << e.what() << std::endl;
        }


        std::vector<std::string> chipConfigs;
        std::string chipConfigPaths;

        if (config["chipType"].empty() || config["chips"].empty()) {
            std::cerr << __PRETTY_FUNCTION__ << " : invalid config, chip type or chips not specified!" << std::endl;
            return 0;
        } else {
            chipType = config["chipType"];
            std::cout << "Chip Type: " << chipType << std::endl;
            std::cout << "Found " << config["chips"].size() << " chips defined!" << std::endl;
            // Loop over chips
            for (unsigned i=0; i<config["chips"].size(); i++) {
                std::cout << "Loading chip #" << i << std::endl;
                try { 
                    json chip = config["chips"][i];
                    std::string chipConfigPath = chip["config"];
                    // TODO should be a shared pointer
                    if (chip["enable"] == 0) {
                        std::cout << " ... chip not enabled, skipping!" << std::endl;
                    } else {
                        bookie.addFe(StdDict::getFrontEnd(chipType).release(), chip["tx"], chip["rx"]);
                        bookie.getLastFe()->init(&*hwCtrl, chip["tx"], chip["rx"]);
                        std::ifstream cfgFile(chipConfigPath);
                        if(chipConfigPaths=="") { 
                            chipConfigPaths = chipConfigPaths + chipConfigPath;
                        } else {
                            chipConfigPaths = chipConfigPaths + "\n" + chipConfigPath;
                        }
                        std::stringstream cfgFile_buffer;
                        if (cfgFile) {
                            // Load config
                            std::cout << "Loading config file: " << chipConfigPath << std::endl;
                            cfgFile_buffer << cfgFile.rdbuf();
                            cfgFile.clear();
                            cfgFile.seekg(0);
                            json cfg = json::parse(cfgFile);
                            dynamic_cast<FrontEndCfg*>(bookie.getLastFe())->loadConfig(cfg);
                            chipConfigs.push_back(cfgFile_buffer.str());
                            cfgFile.close();
                        } else {
                            std::cout << "Config file not found, using default!" << std::endl;
                        }
                        success++;
                        // Save path to config
                        std::size_t botDirPos = chipConfigPath.find_last_of("/");
                        feCfgMap[bookie.getId(bookie.getLastFe())] = chipConfigPath;
                        std::string backupConfigFilePath = chipConfigPath.substr(botDirPos, chipConfigPath.length());

                        // Create backup of current config
                        // TODO fix folder
                        std::ofstream backupCfgFile(outputDir.Value() + backupConfigFilePath + ".before");
                        json backupCfg;
                        dynamic_cast<FrontEndCfg*>(bookie.getLastFe())->writeConfig(backupCfg);
                        backupCfgFile << std::setw(4) << backupCfg;
                        backupCfgFile.close();
                    }	
                } catch (json::parse_error &e) {
                    std::cerr << __PRETTY_FUNCTION__ << " : " << e.what() << std::endl;
                }
            }
        }

        bookie.initGlobalFe(StdDict::getFrontEnd(chipType).release());
        bookie.getGlobalFe()->makeGlobal();
        bookie.getGlobalFe()->init(&*hwCtrl, 0, 0);


        std::cout << "\033[1;31m#######################\033[0m" << std::endl
            << "\033[1;31m##   Creating Scan   ##\033[0m" << std::endl
            << "\033[1;31m#######################\033[0m" << std::endl;
        // Constrcut scan
        std::unique_ptr<ScanBase> s ( nullptr );

        std::cout << "-> Found Scan config, constructing scan ..." << std::endl;
        s.reset( new ScanFactory(&bookie, NULL) );
        std::ifstream scanCfgFile(scanType.Value());
        if (!scanCfgFile) {
            std::cerr << "#ERROR# Could not open scan config: " << scanType.Value() << std::endl;
            throw("buildScan failure!");
        }
        std::stringstream scanCfgFile_buffer;
        scanCfgFile_buffer << scanCfgFile.rdbuf();
        scanCfgFile.clear();
        scanCfgFile.seekg(0);        
        json scanCfg;
        try {
            scanCfg = json::parse(scanCfgFile);
        } catch (json::parse_error &e) {
            std::cerr << "#ERROR# Could not parse config: " << e.what() << std::endl;
        }
        dynamic_cast<ScanFactory&>(*s).loadConfig(scanCfg);
        unsigned numOfTrig = 16;
        if (!scanCfg["scan"]["loops"][0]["config"]["trigMultiplier"].empty()) {
            numOfTrig = scanCfg["scan"]["loops"][0]["config"]["trigMultiplier"];
        }

        std::cout << "Trigger Multiplier: " << numOfTrig << std::endl;
        
        // Loading module config file
        std::stringstream modCfgFile_buffer;
        if (modCfg.Value() == "") {
            std::cout << "No module config given, assuming they are all SCCs." << std::endl;
            //construct single chip module config
            return -1;
        } else {
            // Open module config file
            std::cout << "-> Opening module config: " << modCfg.Value() << std::endl;
            std::ifstream modCfgFile(modCfg.Value());
            if (!modCfgFile) {
                std::cerr <<"#ERROR# Cannot open module config file: " << modCfg.Value() << std::endl;
                return -1;
            }

            modCfgFile_buffer << modCfgFile.rdbuf();
            modCfgFile.clear();
            modCfgFile.seekg(0);

            json modCfg;
            try {
                modCfg = json::parse(modCfgFile);
            } catch (json::parse_error &e) {
                std::cerr << "#ERROR# Could not parse module config: " << e.what() << std::endl;
                return 0;
            }
            //std::string controller = ctrlCfg["ctrlCfg"]["type"];
            //check whether the module config is sane...

        }        
        

        // Object for producer:
        // HwController hwCtrl
        // Bookkeeper bookie
        // ScanBase s


        // Create a producer
        YarrProducer producer(&*hwCtrl, &bookie, &*s, chipType, prodId.Value(), rctrl.Value(), numOfTrig, conCfg.Value(), gConfig_buffer.str(), chipConfigPaths, chipConfigs, ctrlCfgPath.Value(), ctrlCfgFile_buffer.str(), scanType.Value(), scanCfgFile_buffer.str(), modCfg.Value(), modCfgFile_buffer.str());

        // And set it running...
        EUDAQ_INFO("YarrProducer: going into run loop");
        producer.ReadoutLoop();
        // When the readout loop terminates, it is time to go
        std::cout << "~~~ Quitting ~~~" << std::endl;
    } catch (...) {
        // This does some basic error handling of common exceptions
        return op.HandleMainException();
    }
    return 0;
}

EudetArchiver::~EudetArchiver() {
    if (m_curEvent)
        delete m_curEvent;
}


void EudetArchiverRD53a::processEvent(FrontEndData *data) {
    for (auto eventIt = (data->events).begin(); eventIt!=data->events.end(); ++eventIt) {   
        FrontEndEvent thisEvent = *eventIt;

        //std::cout << "[" << m_id << "] Event: tag(" << thisEvent.tag << ") l1id(" << thisEvent.l1id << " bcid(" << thisEvent.bcid << ") hits (" << thisEvent.nHits << ")" << std::endl; 
        
        // Combine events from the same eudet trigger
        // Some special cases:
        // - there are occasional events like this: L1ID(0) BCID(0) TAG(0)
        // - there are other cases where events seem to have continuing BCID, while they should not
        //if (l1ToTag[thisEvent.l1id%m_numOfTrig] != thisEvent.tag || 
        //        ((thisEvent.l1id%m_numOfTrig >= m_numOfTrig-2) && thisEvent.tag == 0))
        //    continue; // Skip bad events


        if (thisEvent.l1id != (lastL1+1)%32) {
            std::cout << "[" << m_id << "] Register lingering event at :" << m_curEvent << " with current l1id (last l1id): " << thisEvent.l1id << "(" << lastL1 << ")" << std::endl;
            continue; // Skip lingering events
        }

        lastL1 = thisEvent.l1id;
 
        // Fill event with data
        unsigned it = m_curEvent->size();
        unsigned bufferSize = sizeof(uint32_t) + (3*sizeof(uint16_t)) + (thisEvent.nHits*sizeof(FrontEndHit));
        m_curEvent->resize(m_curEvent->size() + bufferSize);
        //unsigned char *buffer = new unsigned char[bufferSize];

        *(uint32_t*)&m_curEvent->at(it) = thisEvent.tag; it+= sizeof(uint32_t);
        *(uint16_t*)&m_curEvent->at(it) = thisEvent.l1id; it+= sizeof(uint16_t);
        *(uint16_t*)&m_curEvent->at(it) = thisEvent.bcid; it+= sizeof(uint16_t);
        *(uint16_t*)&m_curEvent->at(it) = thisEvent.nHits; it+= sizeof(uint16_t);
        for (auto &hit : thisEvent.hits) {
            *(FrontEndHit*)&m_curEvent->at(it) = hit; it+= sizeof(FrontEndHit);
        }

        m_curBlock++;
        if (m_curBlock == m_numOfTrig) {
            m_prod->eventMutex.lock();
            m_prod->sendHitData(m_id, m_curEvent);
            m_prod->eventMutex.unlock();
            m_curEvent = new std::vector<char>;
            m_curBlock = 0;
        }
        
    }
}


void EudetArchiverRD53b::processEvent(FrontEndData *data) {
    //std::cout << "[" << m_id << "] Processing " << data->events.size() << " events " << std::endl;
    for (auto eventIt = (data->events).begin(); eventIt!=data->events.end(); ++eventIt) {   
        FrontEndEvent thisEvent = *eventIt;

        //std::cout << "[" << m_id << "] Event: tag(" << thisEvent.tag << ") l1id(" << thisEvent.l1id << " bcid(" << thisEvent.bcid << ") hits (" << thisEvent.nHits << ")" << std::endl;

        // New event started before prev. finished, send old one anyway
        if (thisEvent.tag == 0 && m_curBlock > 0) {
            std::cout << "[" << m_id << "] Event #" << m_eventNumber << " LastL1 " << lastL1 <<  " event index " << (eventIt - (data->events).begin()) << " unfinished, sending with " << m_curBlock << " fragments!" << std::endl;
            m_prod->eventMutex.lock();
            m_prod->sendHitData(m_id, m_curEvent);
            m_prod->eventMutex.unlock();
            m_curEvent = new std::vector<char>;
            m_curBlock = 0;
            m_eventNumber++;
        }


        // Fill event with data
        unsigned it = m_curEvent->size();
        if (lastL1 == thisEvent.tag) {
    	   std::cout << "[" << m_id << "] Vector size " << data->events.size() << " events current index " << (eventIt - (data->events).begin()) << std::endl;
           std::cout << "[" << m_id << "] Detected split tag in event #" << m_eventNumber << " at tag " << thisEvent.tag << " merging hits ..." << std::endl;
           std::cout << "[" << m_id << "] Event: tag(" << thisEvent.tag << ") l1id(" << thisEvent.l1id << " bcid(" << thisEvent.bcid << ") hits (" << thisEvent.nHits << ")" << std::endl;
           if (eventIt == data->events.begin()) continue; // if it's the first element of the vector then the data to merge into is already sent and lost
	   unsigned bufferSize = (thisEvent.nHits*sizeof(FrontEndHit));
           m_curEvent->resize(m_curEvent->size() + bufferSize);
           unsigned cur_hits;
           try {
           	cur_hits = *(uint16_t*)&m_curEvent->at(last_nHits_it);
           }
           catch (const std::out_of_range& oor) {
           	std::cout << "Out of range access in split event merging" << std::endl;
           	std::cout << "Attempted to access index " << last_nHits_it << std::endl;   
           	std::cout << "Event: tag(" << thisEvent.tag << ") l1id(" << thisEvent.l1id << " bcid(" << thisEvent.bcid << ") hits (" << thisEvent.nHits << ")" << std::endl;
           	continue;
           }
           *(uint16_t*)&m_curEvent->at(last_nHits_it) = cur_hits + thisEvent.nHits;
           for (auto &hit : thisEvent.hits) {
           	*(FrontEndHit*)&m_curEvent->at(it) = hit; it+= sizeof(FrontEndHit);
           }
           m_curBlock++;
        } else {
            //std::cout << "Event: tag(" << thisEvent.tag << ") l1id(" << thisEvent.l1id << " bcid(" << thisEvent.bcid << ") hits (" << thisEvent.nHits << ")" << std::endl; 
            unsigned bufferSize = sizeof(uint32_t) + (3*sizeof(uint16_t)) + (thisEvent.nHits*sizeof(FrontEndHit));
            m_curEvent->resize(m_curEvent->size() + bufferSize);

            *(uint32_t*)&m_curEvent->at(it) = thisEvent.tag; it+= sizeof(uint32_t);
            *(uint16_t*)&m_curEvent->at(it) = (uint16_t) m_eventNumber; it+= sizeof(uint16_t);
            *(uint16_t*)&m_curEvent->at(it) = thisEvent.bcid; it+= sizeof(uint16_t);
            *(uint16_t*)&m_curEvent->at(it) = thisEvent.nHits; 
            last_nHits_it = it; it+= sizeof(uint16_t);
            for (auto &hit : thisEvent.hits) {
                *(FrontEndHit*)&m_curEvent->at(it) = hit; it+= sizeof(FrontEndHit);
            }
            m_curBlock++;
        }

        lastL1 = thisEvent.tag;

	if (std::next(eventIt) != data->events.end()) {
		if ( std::next(eventIt)->tag == thisEvent.tag ) {
        		std::cout << "[" << m_id << "] Detected split tag in event #" << m_eventNumber << " at tag " << thisEvent.tag << " event index " << (eventIt - (data->events).begin()) << " rewind curBlock" << std::endl;
			m_curBlock--;
		}
	}	
        if (m_curBlock == m_numOfTrig) {
            m_prod->eventMutex.lock();
            m_prod->sendHitData(m_id, m_curEvent);
            m_prod->eventMutex.unlock();
            m_curEvent = new std::vector<char>;
            m_curBlock = 0;
            m_eventNumber++;
        }

    }
}
