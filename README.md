![https://gitlab.cern.ch/YARR/YarrEudaqProducer/pipelines](https://gitlab.cern.ch/YARR/YarrEudaqProducer/badges/master/pipeline.svg)

# YARR-EUDAQ Producer

EUDAQ producer to take data with the YARR system

## Requiremets

- cmake version 3 or higher
- YARR requirements
- EUDAQ requirements

## Installation

Checkout the repository:
```
$ git clone https://gitlab.cern.ch/YARR/YarrEudaqProducer.git
```

To compile the software run:
```
$ cd YarrEudaqProducer
$ mkdir build
$ cd build
$ cmake3 ..
$ make install -j5
$ cd ..
```

## Run the producer

Run the producer as such:
```
bin/YarrProducer -h configs/controller/specCfg-rd53a-4x3-tlu.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/std_exttrigger.json -r 127.0.0.1
```
- ``-r`` is the run control network address
- ``-l`` eudaq log level
- ``-h`` is the controller config, it should include a trigger logic configuration part. It is important that the mask is set to 16 and to have the deadtime set sufficiently high (~3000) as otherwise there might be glitches in the handshake. All other settings are meaningless for operation with the EUDAQ TLU.
- ``-c`` is the connectivity, the chip mentioned in the connectivity should point to your chip config which you got after tuning and masking
- ``-s`` is the scan config, in there you can overwrite the LatencyConfig (time in between hit and when trigger should arrive)
- ``-m`` is the module config
- ``-o`` output directory for scan files written by the producer
- ``-p`` producer ID; be default 0; has to be differently selected for each instance of the yarr producer running simultaneously

Please take note that as usual for a eudaq derived producer the "HOSTNAME" environmental variable has to be (network call back address of the producer; it will be used by the run control and appears in the cconnection list of the run control; localhost/127.0.0.1 will not work if they are not running on the same computer).

All of these files are loaded into memory at the beginning of the program. The EUDAQ configure step does not reload these files. To change parameters it is recommend to restart the producer.
