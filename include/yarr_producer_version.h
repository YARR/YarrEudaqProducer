#ifndef YARR_PRODUCER_VERSION_H
#define YARR_PRODUCER_VERSION_H
#include <string>
#include "storage.hpp"

namespace yarr_producer {
namespace version {
    std::string getYarrProducerVersion();
    unsigned getYarrProducerVersionMajor();
    unsigned getYarrProducerVersionMinor();
    unsigned getYarrProducerVersionPatch();
    unsigned getYarrProducerVersionTweak();
    json get();
}
}
#endif
